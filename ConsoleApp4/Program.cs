﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 32, 321, 515, 23, 22, 2 };
            NumberSequence test = new NumberSequence(array);

            SortStrategy SortTest1 = new CombSort();
            test.SetSortStrategy(SortTest1);
            test.Sort();
            Console.Write(test.ToString());
            Console.WriteLine();

            SortStrategy SortTest2 = new BubbleSort();
            test.SetSortStrategy(SortTest2);
            test.Sort();
            Console.Write(test.ToString());
            Console.WriteLine();

            SortStrategy SortTest3 = new SequentialSort();
            test.SetSortStrategy(SortTest3);
            test.Sort();
            Console.Write(test.ToString());
            Console.WriteLine();


            SearchStrategy SearchTest1 = new BinarySearch();
            test.SetSearchStrategy(SearchTest1);
            Console.WriteLine(test.Search(22));
        }
    }
}
